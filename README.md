

														=================
														   OCAS ChatBOT
														==================

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											

								 
# Dialogflow (Api.ai) - sample webhook implementation in Python Flask.

#This is a really simple webhook implementation that gets Dialogflow classification JSON (i.e. a JSON output of Dialogflow /query endpoint) 
and returns a fulfillment response.

# Using this powerful web service bot can respond to users collecting information from external web services even Databases!!

More info about Dialogflow webhooks could be found here:
[Dialogflow Webhook](https://dialogflow.com/docs/fulfillment#webhook)

# Deploy to:
[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

# What does the service do?

This service can provide business operation data of OCAS to auhtenticated users by querying databases while chatting with the ChatBOT.

Regarding three tier architecture this application actually works as a loosely coupled middleware and maintains interactkon with Dialogflow using Webhook Protocol 
and communcate RESTFUL Web Services with python Request-Response mechanism. All these three tier applications have to work harmonically 
for completing a successfull response to a user.

Example:
User: How many request has been submitted during last week in Gulshan Baranch ? 
Bot: Number of applications submitted during last week in Gulshan Branch: 13. 

After fulfilling proper authentications bot can provide real time data to the users.  

#Features:

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
Regards, 

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	

